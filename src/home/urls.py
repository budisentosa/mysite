from django.urls import path, re_path

from . import views
from accounts import views as account_views

app_name = 'home'
urlpatterns = [
    re_path(r'^$', views.IndexView.as_view(), name='index'),
    path('<username>/', account_views.redirect_view, name='user_redirect'),
]

