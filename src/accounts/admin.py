from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from .models import UserProfile
from .forms import *


class UserAdmin(auth_admin.UserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('first_name', 'last_name')}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
        ('User profile', {'fields': ('bio', )}),
    )
admin.site.register(UserProfile, UserAdmin)
