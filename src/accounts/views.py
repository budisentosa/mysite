from django.shortcuts import render, redirect, get_object_or_404
from .forms import LoginForm
from django.contrib.auth import authenticate, login
from django.http import HttpResponse
from accounts.models import UserProfile
from django.views import generic
from home.views import CustomView
from blog.models import Post
from django.contrib.auth.decorators import login_required

def redirect_view(request, username):
    user = get_object_or_404(UserProfile, username=username)
    return redirect('accounts:view', pk=user.pk)

def new_account(request):
    return HttpResponse("new account")

def edit_account(request):
    return HttpResponse("edit account")

@login_required
def my_account(request):
    return redirect('accounts:view', pk=request.user.pk)

class UserInfoView(CustomView, generic.DetailView):
    model = UserProfile
    context_object_name = 'userprof'
    template_name = 'accounts/profile.html'

    def get_more_context(self):
        return {'title': "profile", 'page': "info", 'posts': Post.objects.all().filter(author_id=self.kwargs['pk'])}


class UserBlogInfoView(CustomView, generic.ListView):
    model = UserProfile
    context_object_name = 'posts'
    template_name = 'accounts/profile.html'

    def get_queryset(self):
        qs = Post.objects.all().filter(author_id=self.kwargs['pk'])
        return qs
    def get_more_context(self):
        return {'title': "profile", 'page': "blog", 'userprof': UserProfile.objects.filter(pk=self.kwargs['pk']).first()}


